<!-- TOC -->

- [1. 断言脚本](#1-断言脚本)

<!-- /TOC -->

# 1. 断言脚本
Postman断言脚本在Tests中填写。可以在断言脚本中自定义检查response中的内容。          
![断言](http://sunyanping.gitee.io/it-keep//ASSET/Postman-断言.jpg)

一般常用的断言脚本中，如上图中所示，可以直接选择然后按照自己的实际情况修改脚本来使用。
常用的脚本列举几个如下：

- 断言返回状态码
```
pm.test("Status code is 200", function () {
    pm.response.to.have.status(200);
});
```

- 断言response中是否包含某个字符串
```
pm.test("Body matches string", function () {
    pm.expect(pm.response.text()).to.include("string_you_want_to_search");
});
```

- 断言json格式的response中value的值是否为100
```
pm.test("Your test name", function () {
    var jsonData = pm.response.json();
    pm.expect(jsonData.value).to.eql(100);
});
```

- 断言response是否等于某个字符串
```
pm.test("Body is correct", function () {
    pm.response.to.have.body("response_body_string");
});
```

- 断言status返回201，202都是成功
```
pm.test("Successful POST request", function () {
    pm.expect(pm.response.code).to.be.oneOf([201, 202]);
});
```

