<!-- TOC -->

- [常用快捷键](#常用快捷键)
- [自己喜欢的插件](#自己喜欢的插件)
- [自己喜欢的设置](#自己喜欢的设置)

<!-- /TOC -->
# 常用快捷键
```
Ctrl+P          // 进入ctrl+p模式，可以干很多事情
Ctrl+Shift+N        // 打开一个新窗口
Ctrl+Shift+W        // 关闭当前窗口
Ctrl+Tab        // 文件之间进行切换
Ctrl+End/Home   // 光标移动到文件结尾/开头
Alt+Up/Down      // 向上/下移动一行
Shift+Alt+↑/↓       // 向上/下复制一行
Ctrl+Enter      // 在当前行下边插入一行
Ctrl+Shift+Enter        // 在当前行上方插入一行
Ctrl+Delete     // 删除光标后的所有字符
Alt+Shift+鼠标左键      // 多行编辑
Ctrl+H      // 查找替换
Ctrl+Shift+F        // 整个文件夹中查找
Shift+Home      // 选中光标到行首内容
Shift+End       // 选中光标到行尾内容
Ctrl+Shift+G        // 显示Git

```

# 自己喜欢的插件

- `Auto Close Tag`
- `Beautify`
- `Chinese(Simplified) Language Pack For VSCode`
- `ESLint`
- `GitLens`
- `Material Icon Theme`
- `Vuter`
- `vscode-icons`
- `Markdown TOC`
- `Markdown Editor`

# 自己喜欢的设置
```
{
    "terminal.integrated.shell.windows": "C:\\Program Files\\Git\\bin\\bash.exe",
    "editor.fontFamily": "Fira Code, Consolas,'Courier New',monospace, ",
    "terminal.integrated.fontFamily": "monospace", // 终端字体
    "workbench.iconTheme": "vscode-icons",
    "markdown.preview.lineHeight": 2,
    "editor.lineHeight": 25,
    "editor.rulers": [100]
}

{
    "terminal.integrated.shell.windows": "C:\\Program Files\\Git\\bin\\bash.exe",
    "editor.fontFamily": "'Fira Code', 'Courier New', Consolas,monospace, ",
    "terminal.integrated.fontFamily": "monospace", // 终端字体
    "workbench.iconTheme": "material-icon-theme",
    "markdown.preview.lineHeight": 2,
    "editor.lineHeight": 25,
    "editor.rulers": [100],
    "editor.tabSize": 2,
    <!-- "prettier.semi": false,  // 格式化代码时，js代码中行结束不使用分号
    "prettier.singleQuote": true, // 格式化代码时，js代码中使用单引号 -->
    "vetur.format.defaultFormatterOptions": {  //安装vetur插件，格式化代码时使用vetur，解决格式化时js代码中的单引号变双引号问题、自动添加分号问题
        "prettier": {
            "singleQuote": true,
            "semi": false,
        }
    },
    "files.eol": "\n",        // 行尾的换行符，当时为了解决markdown toc插件生成的目录中是auto的问题
}
```
