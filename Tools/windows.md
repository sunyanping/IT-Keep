<!-- TOC -->

- [windows常用命令](#windows常用命令)
- [局域网内共享文件夹](#局域网内共享文件夹)
- [在windows电脑上部署java应用](#在windows电脑上部署java应用)

<!-- /TOC -->

# windows常用命令
```
netstat -aon  // 查找所有端口
netstat -aon | findstr "8080" // 查找8080端口的程序
tasklist | findstr "PID"  // 列出进程号对应的所有子进程
taskkill /f /t /im 任务名称  // 杀掉执行的任务
```

# 局域网内共享文件夹

1. 设置文件夹共享属性  
2. `win+R`调出运行窗口，输入`\\<ip_addr>`，然后点击确定，接着弹出的磁盘界面，这里展示的便是指定的局域网地址内共享的文件夹

# 在windows电脑上部署java应用

可以参考以下脚本
```
@echo off

:: 设置一些参数
set CMD_HOME=C:\Users\Administrator
set JAVA_HOME=D:\HLY\ProgramFiles\Java\jdk1.8.0_231
set PATH=%JAVA_HOME%/bin;%JAVA_HOME%/jre/bin;%CMD_HOME%
set CLASSPATH=.;%JAVA_HOME%\lib\dt.jar;%JAVA_HOME%\lib\tools.jar

::应用包所在目录
set APP_HOME=D:\HLY\app

:: 查找正在运行的应用，并kill掉
tasklist | findstr /i hly-brbiotech.exe && taskkill /f /im hly-brbiotech.exe

:: 启动应用
echo "开始启动应用..."
start hly-brbiotech -Dfile.encoding=GBK -Xms256M -Xmx512M -jar %APP_HOME%\brbiotech.war --spring.profiles.active=uat --server.port=8089
```

1. 设置环境变量。在脚本中设置环境变量只会在当前的命令行生效，不会对全局产生影响。
2. 启动应用：通常情况下可以使用两种命令启动java程序，`start java -jar xxxx.jar` 和 `start javaw -jar xxxx.jar` 。前一种是前台进程的方式，后一种是后台进程的方式。 
前台进程的方式启动如果关闭打开的命令行窗口，则进程也会停掉。
3. 查找运行的进程，可以使用自定义进程名称的方式。为了方便找到运行的程序，我们启动时可以自定义进程，在jdk的/bin目录下找到 java.exe 和 javaw.exe 文件，我们默认情况下就是使用这两个程序来启动的，默认的进程名称就是java 和 javaw。我们可以将该文件复制一份分别命名为 demo.exe 和 demow.exe ，然后就可以使用`start demo -jar xxxx.jar` 和 `start demow -jar xxxx.jar` 的命令来启动，启动的进程名称就是demo和demow。
