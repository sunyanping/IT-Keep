<!-- TOC -->

- [bash常用快捷键](#bash常用快捷键)
- [curl](#curl)
- [vi](#vi)
- [grep](#grep)
- [awk](#awk)
- [shell脚本第一行的`#!/bin/bash`和`#!/bin/sh`](#shell脚本第一行的binbash和binsh)
- [执行脚本的几种方式](#执行脚本的几种方式)
- [export](#export)
- [dirname](#dirname)
- [EOF](#eof)
- [Linux文件信息查看 `ls -l`](#linux文件信息查看-ls--l)
- [`chmod`](#chmod)
- [echo](#echo)
- [端口查看](#端口查看)
- [常用于查看服务器配置](#常用于查看服务器配置)
- [解压缩](#解压缩)
  - [unzip](#unzip)
  - [tar](#tar)
- [sleep](#sleep)
- [服务器时间](#服务器时间)
- [shell 常用示例](#shell-常用示例)
  - [快速清空文件的方法](#快速清空文件的方法)
  - [获取当前所在公网IP地址](#获取当前所在公网ip地址)
  - [历史命令使用技巧](#历史命令使用技巧)

<!-- /TOC -->
# bash常用快捷键
```bash
Ctrl+A  # 将光标移动到行首
Ctrl+B   # 光标后退一位
Ctrl+D  # 删除光标下的一个字符
Ctrl+E  # 将光标移动到行尾
Ctrl+F  # 光标向前移动一个字符
Ctrl+L  # 清屏，和clear命令同作用
Ctrl+U  # 删除光标前所有的内容
Ctrl+K  # 删除光标后所有内容
Ctrl+Y  # 可以将上一步Ctrl+U清除的内容粘贴回来
Alt+U  # 将光标位置到词尾的所有字符转为大写
Alt+L  # 将光标位置到词尾的所有字符转为小写
Alt+B  # 光标向前移动一个单词
Alt+F   # 光标向后移动一个单词
Atl+D  # 剪切光标所在处下一个单词
```

# curl
*用于传输来自服务器或者到服务器数据的工具*  
当指定的url没有协议类型时默认使用的是http协议。当没有指定请求方法时默认是get方式。

- `-#, --progress-bar`：curl进度显示为一个进度条，而不是标准的进度表
- `-a, --append`: (FTP/SFTP)当在上传中使用，告诉curl追加到目标文件而不是覆盖它，如果文件不存在将创建它。
- `-d, --data`: 使用该选项默认请求方式是post，指定向服务器发送的数据。这和提交表单的按钮是同样的，默认curl使用content-type application/x-www-form-urlencoded将数据传输给服务器。也可以使用`-F, form`指定。如果这些命令在同一个命令行使用多次，这些数据片段将使用指定的分隔符`&`合并。如果以`@`开始数据，则后面的应该跟的是一个文件名，一便读取文件中的数据。
- `-e, --referer`: 将从哪个页面跳转过来的信息发送到服务器。例如：`curl -e 'http://www.baidu.com' http://host:port/api`
- `-f, --fail`: 静默失败
- `-G, --get`: 指定get请求方式
- `-H, --header`: 定义请求头，例如：`curl -H 'Connection: keep-alive' -H 'Referer: https://sina.com.cn' -H 'User-Agent: Mozilla/1.0' http://www.zhangblog.com/2019/06/24/domainexpire/`
- `-o, --output`: 输出到一个文件，而不是标准输出
- `-s, --silent`: 静默
- `-S, --show-error`: 和`-s`一起使用时，如果出现失败，则失败信息会显示
- `-X, --request`: 指定请求方式
- FTP下载：`curl -O ftp://ftp1:123456@172.16.1.195:21/tmpdata/tmp.data`或者`curl -O -u ftp1:123456 ftp://172.16.1.195:21/tmpdata/tmp.data`
- `-T, --upload-file`: FTP文件上传，`curl -T tmp_client.data ftp://ftp1:123456@172.16.1.195:21/tmpdata/`或者`curl -T tmp_client.data -u ftp1:123456 ftp://172.16.1.195:21/tmpdata`
- `-k`: 使用此参数可以忽略证书认证curl https

*example*
```bash
# POST请求注意参数是json需要放在body传递时必须设置Header(Content-Type: application/json)
curl -X POST http://localhost/  -H 'Content-Type: application/json' -H 'Toekn: 123' -d '{"name": "张三", "age": 10'  # header传多对值；requestbody
curl -X POST http://localhost -d 'name=张三&age=10'  # querystring的方式
```

# vi
- 命令模式
```bash
/str    # 向下匹配字符串str，按n光标移到下一个匹配的字符串处
?str    # 向上匹配字符串str，按n光标移到上一个匹配的字符串处
yyp   # 复制当前行到下一行
dd    # 删除当前行
shift+g   # 跳到文本的最后一行
gg    # 跳到文本的第一行
dG    # 使用gg先跳到文本的第一行，再使用dG，可以清除文本的内容
yy    # 复制光标所在行整行
y^    # 复制光标所在处至行首，包含光标所在处字符
y$    # 复制光标所在处至行尾，包含光标所在处字符
yG    # 复制至文档尾
y1G     # 复制至文档首  
```

- 输入模式（使用`i`进入）
```
```

- 底线命令模式/编辑模式（使用`:`进入）
```bash
:%s/<str>//gn   # 匹配整个文件中的str字符串，显示匹配到多少个，可以用来查看统计
```

# grep
```bash
grep -E "a|b"   # 匹配a或者b，注意一定是大写E参数。此也是grep的OR匹配方式
grep -E "a.*b"    # 匹配a或者b字符串，注意a,b两者有顺序。其实质上匹配的是类似于: a*****b 这样的字符串。此也是grep的AND匹配方式。
grep -E "a.*b|b.*a"   # 弥补匹配a或者b时的顺序问题。
grep <str> <file>   # 查找file中和str匹配的内容，按照行显示
grep -i <str> # 忽略大小写
grep -v <str>   # grep的NOT操作，反选匹配。可以使用该命令忽略grep本身这条语句。

```

- grep 关键字后面的字符串中存在特殊字符时使用单引号

# awk
```bash
awk '{print $1}'    # 截取第一列，注意一定是单引号
```

# shell脚本第一行的`#!/bin/bash`和`#!/bin/sh`
在shell脚本中第一行往往有这样一行代码，这虽然是`#`开头但是它不是注释，这行代码是用来指定执行脚本文件时使用哪种解释执行器。
在linux系统中默认使用的是`bash`解释执行器，`bash`是兼容`sh`的。在`#!/bin/sh`脚本中不允许出现任何不在POSIX标准的命令，
比如脚本中包含一些自定义函数等，就需要使用`#!/bin/bash`。  

# 执行脚本的几种方式
- `bash xxx.sh`或者`sh xxx.sh`: 指定解释执行器执行脚本，脚本没有执行权限时，这种方式也可以执行。
- 指定路径执行`./xxx.sh`： 脚本没有执行权限时，这种方式不能执行。
- `source xxx.sh`或者`. xxx.sh`：会将shell中得命令加载在当前shell中进行执行，而不是在子shell进程中执行，所以当前shell中可以使用脚本中定义得变量等。

# export
- 可以设定环境变量，使得该变量不仅仅只是生效于当前得shell中
- 该变量仅仅是指生效于当前登陆
```bash
export INSTALL_HOME=$(
  cd $(dirname $0)
  pwd
)
```

# dirname
```bash
dirname $0    # $0代表输入的第一个参数，这个参数是一个文件或者目录的路径，dirname用来获取该文件或者目录的父目录
dirname /etc/hosts    # 结果是/etc
```

# EOF
EOF是END Of File的缩写,表示自定义终止符.既然自定义,那么EOF就不是固定的,可以随意设置别名。EOF一般会配合cat能够多行文本输出.

example: 将输入输出到文件
```bash
[root@iz2ze0bcvmlsi9ddobp0bpz home]# cat <<eof > test111.txt
> 112
> 122
> 122
> eof
[root@iz2ze0bcvmlsi9ddobp0bpz home]# cat test111.txt
112
122
122
[root@iz2ze0bcvmlsi9ddobp0bpz home]#
```

example: 将内容输出到文件
```bash
cat > /usr/local/mysql/my.cnf << EOF  # 或者cat << EOF > /usr/local/mysql/my.cnf    
[client]
port = 3306
socket = /usr/local/mysql/var/mysql.sock

[mysqld]
port = 3306
socket = /usr/local/mysql/var/mysql.sock
EOF
```

# Linux文件信息查看 `ls -l`
![](http://sunyanping.gitee.io/it-keep/ASSET/linux文件信息查看讲解.png)

`ls -lh <path>`  以K、M、G为单位查看文件大小

# `chmod`
`chmod` 命令用来变更文件或目录的权限。文件或目录权限的控制分别以读取、写入、执行3种一般权限来区分。用户可以使用chmod指令去变更文件与目录的权限，设置方式采用文字或数字代号皆可。
权限范围的表示法如下：  
- `u`, user: 文件/目录的拥有者
- `g`, group: 文件/目录的所属组用户
- `o`, other: 除了u和g的其他用户
- `a`, all: 所有的用户
- `r`, read: 读权限，数字4代表
- `w`, write: 写权限，数字2代表
- `x`, 执行权限，数字1代表
- `-`, 无权限，数字0代表
```bash
chmod 777 bar   # 给bar设置所有用户所有权限，其中权限设置时第一位代表u，第二位代表g，第三位代表o
chmod 751 bar   # 给bar文件设置：u-所有权限，g-读和执行权限，o-执行权限
chmod a+x   # 给所有用户执行权限
chmod u+rwx,g+rx,o+-  # 给u所有权限，g读写权限，o-无权限
```

# echo
1. 重定向到文件
```bash
echo <str> > <file>   # 重定向输出str到文件file，会覆盖文件原内容
echo <str> >> <file>    # 重定向输出str到文件file，添加到原内容之后
```
2. 输出变量：`echo $var`

# 端口查看
1. lsof：`lsof -i:端口号`
2. netstat：`netstat -tunlp | grep 端口号 `，用于显示tcp, udp的端口和进程等相关情况

# 常用于查看服务器配置
- 查看操作系统
```bash
uname -a    # 查看当前操作系统内核信息
cat /proc/version    # 查看当前操作系统版本
cat /proc/issue  # 查看当前操作系统发行版版本
```
- 查CPU信息
```bash
lscpu   # 查看cpu信息信号
cat /proc/cpuinfo   # 查看物理CPU
```
- 查看内存
```bash
free -m,g   # 默认K为单位显示内存大小，-m,g参数分别以M和G为单位
cat /proc/meminfo   # 内存详细信息
```
- 查看硬盘
```bash
lsblk   # 查看硬盘各分区大小
df -h   # 查看硬盘各卷大小
fdisk -l # 硬盘外设情况
```

# 解压缩
## unzip
```bash
unzip -l test.zip   ## 查看zip压缩文件中包含哪些文件
unzip -t test.zip   # 查看zip文件是否损坏
unzip test.zip    # 解压缩到当前目录
unzip -d /temp test.zip   # 将文件解压缩到指定目录，使用-d参数
unzip -o test.zip -d /tmp/    # 将文件解压缩到指定目录，若指定目录已存在相同的目录进行覆盖
unzip -n test.zip
unzip -n -d /temp test.zip  # 解压缩时不想覆盖已经存在的文件，使用-n参数
```

## tar
```bash
tar –xvf file.tar  #解压 tar包
tar -xzvf file.tar.gz  #解压tar.gz
tar –xZvf file.tar.Z   #解压tar.Z
tar -xjvf file.tar.bz2   #解压 tar.bz2
tar -zxvf file.tar.gz -C /tmp  # 解压文件到指定目录
```

# sleep
sleep可以使用格式`sleep <n>`， n表示睡眠持续时间，可以是整数和小数。默认单位是秒(s)。单位取值有：天(d)/时(h)/分(m)/秒(s)。    
```bash
sleep 10    # 睡眠10s
sleep 7d 2h 3m      # 睡眠7天2小时3分钟，注意中间需要一个空格。
```
**默认情况下，sleep 的进程是不占用 CPU 时间的**

# 服务器时间
```bash
date    # 查看系统时间
hwclock   # 查看硬件时间
timedatectl set-timezone Asia/Shanghai  # 设置系统时区为上海
```

centos中常设置服务器时间方式：
```bash
# 安装utpdate工具
yum -y install utp ntpdate
# 设置系统时间与网络时间同步
ntpdate cn.pool.ntp.org
# 将系统时间写入硬件时间
hwclock --systohc
```

# shell 常用示例
```bash
#!/bin/bash

# 放置应用war的目录
project_home=/app/apps/yanping/united-imaging

# 放置日志的目录
log_home=/mnt/app/logs/united-imaging

# 查找atl的java进程
pid=$(ps -ef| grep java| grep united-imaging| awk '{print $2}')
echo '查找到atl应用java进程ID：'$pid

# kill进程
if ["" != "$pid"] then
  kill -9 $pid
fi

echo '进程已kill'

# 启动新的进程
nohup java -Xms128M -Xmx256M -jar $project_home/united-imaging.war --spring.profile.active=uat --server.port=10177 >/dev/null 2>&1 &
echo '已开始启动新的进程'

# 打开日志查看
tail -f $log_home/united-imaging.log

```

## 快速清空文件的方法
```bash
: > access.log
cp /dev/null access.log
true > access.log
cat /dec/null access.log
echo -n "" > access.log
echo > access.log
truncate -s 0 access.log
```

## 获取当前所在公网IP地址
```bash
curl ip.sb  
```

## 历史命令使用技巧
```bash
!!  # 重复执行上条命令
!N    # 执行history中的第N条命令
!pw   # 执行最近一次的pw开头的命令
!$    # 表示最近一次的命令的最后一个参数
#  比如如下使用
vim /home/access.log
mv !$ !$.back   # 这样就可以轻松将/home/access.log文件重命名为/home/access.log.bak
```

- 在执行的命令前加一个空格，执行的命令不会被添加倒history
- `history -c` 可以清空history中的命令记录
