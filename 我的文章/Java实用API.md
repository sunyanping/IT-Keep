<!-- TOC -->

- [1. StringJoiner: 拼接字符串](#1-stringjoiner-拼接字符串)

<!-- /TOC -->

# 1. StringJoiner: 拼接字符串
可以使用指定的字符拼接字符串，可以解决常用StringBuilder拼接是带来的尾部需要单独处理的问题。
