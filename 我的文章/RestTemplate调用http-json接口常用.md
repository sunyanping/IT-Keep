
`RestTemplate` 由Spring Web模块提供的一个组件，可以作为一个HttpClient访问Rest服务。常用的同类型HttpClient有：JDK（`HttpURLConnection`）、Apache(`HttpClient`)、Square(`OKHttp`)等。

# 使用方法

## POST
- POST请求，发送json格式的请求数据，数据在body中

```java
HttpHeaders headers = new HttpHeaders();
headers.setContentType(MediaType.APPLICATION_JSON);
headers.add("Authorization", "Bearer " + "xxx");

Map<String, Object> requestBody = new HashMap<>();
requestBody.put("name", "张三");
requestBody.put("age", "23");
HttpEntity httpEntity = new HttpEntity<>(requestBody, httpHeaders);
ResponseEntity<Object> response = restTemplate.postForEntity(URL, httpEntity, String.class);
```


- POST请求，发送表单数据，Content-Type: application/x-www-form-urlencoded

```java
// 设置请求头
HttpHeaders headers = new HttpHeaders();
headers.add("Content-Type", "application/x-www-form-urlencoded");
// 设置请求参数
MultiValueMap<String, Object> params = new LinkedMultiValueMap<>();
params.add("name", "张三");
params.add("age", "23");

HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(params, headers);
ResponseEntity<Object> response = restTemplate.postForEntity(URL, httpEntity, String.class);
```


# 处理响应报文中中文显示乱码
```java
@Bean("remoteRestTemplate")
public RestTemplate remoteRestTemplate() {
    RestTemplate template = new RestTemplate();
    // 解决响应报文中文乱码
    List<HttpMessageConverter<?>> httpMessageConverters = template.getMessageConverters();
    httpMessageConverters.forEach(httpMessageConverter -> {
        if (httpMessageConverter instanceof StringHttpMessageConverter) {
            StringHttpMessageConverter messageConverter = (StringHttpMessageConverter) httpMessageConverter;
            messageConverter.setDefaultCharset(StandardCharsets.UTF_8);
        }
    });
    return template;
}
```