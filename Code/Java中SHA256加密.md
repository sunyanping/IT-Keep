<!-- TOC -->

- [MessageDigest Class in Java](#messagedigest-class-in-java)
- [Guava Library](#guava-library)
- [Apache Commons Codecs](#apache-commons-codecs)

<!-- /TOC -->

源文档：https://www.baeldung.com/sha-256-hashing-java

# MessageDigest Class in Java
```java
MessageDigest digest = MessageDigest.getInstance("SHA-256");
byte[] encodedhash = digest.digest(
  originalString.getBytes(StandardCharsets.UTF_8));
```

```java
private static String bytesToHex(byte[] hash) {
    StringBuilder hexString = new StringBuilder(2 * hash.length);
    for (int i = 0; i < hash.length; i++) {
        String hex = Integer.toHexString(0xff & hash[i]);
        if(hex.length() == 1) {
            hexString.append('0');
        }
        hexString.append(hex);
    }
    return hexString.toString();
}
```

# Guava Library
```html
<dependency>
    <groupId>com.google.guava</groupId>
    <artifactId>guava</artifactId>
    <version>20.0</version>
</dependency>
```

```java
String sha256hex = Hashing.sha256()
  .hashString(originalString, StandardCharsets.UTF_8)
  .toString();
```

# Apache Commons Codecs
```html
<dependency>
    <groupId>commons-codec</groupId>
    <artifactId>commons-codec</artifactId>
    <version>1.11</version>
</dependency>
```

```java
String sha256hex = DigestUtils.sha256Hex(originalString);
```