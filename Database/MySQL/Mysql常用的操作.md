<!-- TOC -->

- [一些SQL语句](#一些sql语句)
  - [数据库相关](#数据库相关)
  - [表相关](#表相关)
  - [视图相关](#视图相关)
  - [数据相关](#数据相关)
  - [索引相关](#索引相关)
  - [用户、角色、权限相关](#用户角色权限相关)
- [视图](#视图)
- [其他常用的语句](#其他常用的语句)
  - [数据复制](#数据复制)
  - [存在则更新，不存在则新增](#存在则更新不存在则新增)

<!-- /TOC -->

**推荐常看：[http://c.biancheng.net/mysql/](http://c.biancheng.net/mysql/)**

# 一些SQL语句
这里整理一些常用的**DDL语句（数据库操作语句）**，**DML语句（数据操作语句）**，**DCL语句（用户、角色、权限相关）**。以方便平时的查询和使用。

## 数据库相关
```sql
create database <dbname>;    ## 创建数据库
use <dbname>;   ## 指定使用的数据库
drop database <dbname>;     ## 删除指定的数据库 
```

## 表相关
```sql
create table <tablename> ([column...]);     ## 创建表
drop table <tablename>;     ## 删除表
show create table <tablename> ;     ## 查看表的定义语句
desc <tablename>;    ## 看表结构的详细信息
show fields from <tablename>;    ## 看表结构的详细信息
alter table <tablename> rename <new_tablename>;     ## 修改表名
rename table <tablename> to <new_tablename>;    ## 修改表名
alter table <tablename> comment '这是表的注释';   ## 修改表的注释
alter table <tablename> drop primary key;   ## 删除表的主键（不是删除字段）
ALTER TABLE <tablename> MODIFY COLUMN url LONGTEXT COMMENT '这是列的注释';  ## 修改表字段
```

## 视图相关
```sql
create view <viewname> as <select语句>;    ## 创建视图
alter view <viewname> as <select语句>;   ## 修改视图
```

## 数据相关
```sql
truncate table <tablename>;   ## 清空表中数据
```

使用`truncate`命令清空表数据时，如果表存在外键关系，会提示 `Cannot truncate a table referenced in a foreign key constraint …`，可以使用下面步骤解决。
1. 删除之前先执行:  `SET foreign_key_checks = 0  # 删除外键约束`
2. 删除完之后在执行：`SET foreign_key_checks = 1    # 启动外键约束`


## 索引相关
```sql
show index form <tablename>;   ## 查看表索引
DROP INDEX <index_name> ON <tablename>;   ## 删除表指定索引
```

## 用户、角色、权限相关
```sql
grant all privileges on *.* to 'username'@'%' identified by 'password';   ## 授予用户和root用户同权限
flush privileges;  
```

# 视图
视图（View）是一种虚拟存在的表，同真实表一样，视图也由列和行构成，但视图并不实际存在于数据库中。行和列的数据来自于定义视图的查询中所使用的表，并且还是在使用视图时动态生成的。
实际上视图就是一个查询，从创建视图的sql语句中就可以很好理解。`create view <viewname> as <select语句>; `     

视图优点：    
1. 定制用户数据，聚焦特定的数据
2. 简化数据操作
3. 提高数据的安全性，有助于限制对特定用户的数据访问
4. 数据库视图实现向后兼容

视图缺点：
- 性能：从数据库视图查询数据可能会很慢，特别是如果视图是基于其他视图创建的。
- 表依赖关系：将根据数据库的基础表创建一个视图。每当更改与其相关联的表的结构时，都必须更改视图。

**提示：ORDER BY 子句可以用在视图中，但若该视图检索数据的 SELECT 语句中也含有 ORDER BY 子句，则该视图中的 ORDER BY 子句将被覆盖。**

# 其他常用的语句

## 数据复制  
```sql
insert into table1(c1, c2) select a1,a2 from table2;        // 将table2的数据赋值到table1中，前提是table1需要存在
select count(1) into @a from tablename;     // 将表的数据数复制到变量@a上
create table <new_table> select * from <table>;       // 创建new_table并且将table的数据结构和数据一并复制到new_table
create table <new_table> select * from <table> where 1=2;   // 创建新表new_table,并且保持和table的数据结构一致
```

## 存在则更新，不存在则新增  
通常我们在做这个需求的时候会考虑先查询，根据唯一字段或者其他唯一的标识来查询表中的数据数目，当数量大于0是，则进行更新，当数量等于0时则进行新增。所以一般包含2步。现在总结几种只需要一步就可以实现的方法。  

以下几种方式都需要遵循要求：
-  **使用此种方式的前提必须有主键或者有唯一索引。原理就是根据主键/唯一索引来判断进行新增/更新操作**
- **作为唯一索引的字段（单个或者多个）不允许为空**

1.  使用`on duplicate key update`语句
```sql
insert into customer(name, id_card, phone) values('张三', '1234', '110')
on duplicate key 
update phone = '120';
```
（1）当表中没有数据执行新增时，影响的数据行数是1；当表中有主键/唯一索引相同的数据时，并且更新的字段值和当前数据表中的数据不同时，影响的数据行数是；当表中有主键/唯一索引相同的数据时，并且更新的字段值和当前数据表中的数据相同时，影响的数据行数是0；

2. 使用`replace`  
`replace`相当于包含了2部分的操作：`delete`和`insert`。但是`replace`是一个原子操作。
```sql
replcae into customer(name, id_card, phone) values('lisi', '23332'，'23233' );
```
（1）当表中没有数据执行新增时，影响的数据行数是1；当表中有主键/唯一索引相同的数据时，并且更新的字段值和当前数据表中的数据不同时，影响的数据行数是；当表中有主键/唯一索引相同的数据时，并且更新的字段值和当前数据表中的数据相同时，影响的数据行数是1；

3. 使用`insert ignore`  
这个和上面2个不同，这个是：不存在则新增，存在则忽略插入，而不是更新。
```sql
inert ignore into customer(name, id_card, phone) values('huan', '23334', '23424');
```


