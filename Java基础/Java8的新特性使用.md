<!-- TOC -->

- [函数式接口（`@FunctionalInterface`）](#函数式接口functionalinterface)
- [Lambda表达式](#lambda表达式)
  - [Lambda表达式一些应用的场景](#lambda表达式一些应用的场景)
    - [循环遍历](#循环遍历)
    - [参数的传递行为并不仅仅是传值](#参数的传递行为并不仅仅是传值)
- [Optional对象](#optional对象)
- [内置的函数式接口](#内置的函数式接口)
- [Base64的编码与解码](#base64的编码与解码)
- [Date API](#date-api)
  - [Clock](#clock)
  - [日期（LocalDate）相关](#日期localdate相关)
  - [日期和时间（LocalTime和LocalDateTime）](#日期和时间localtime和localdatetime)
  - [时区和偏移（Zone 和 Offset）](#时区和偏移zone-和-offset)
    - [ZoneId 和 ZondeOffset](#zoneid-和-zondeoffset)
    - [日期和时间（ZoneDateTime、OffsetDateTime、OffsetTime）](#日期和时间zonedatetimeoffsetdatetimeoffsettime)
      - [ZonedDateTime](#zoneddatetime)
      - [OffsetDateTime](#offsetdatetime)
      - [OffsetTime](#offsettime)

<!-- /TOC -->

# 函数式接口（`@FunctionalInterface`）
因为函数式编程是大势所趋，所以在Java8中引入了例如lambda等一些新的特性，使得可以将函数作为参数进行传递。而函数式接口是为了更好的使用lambda表达式而设计的一种解决方案。
“函数式接口”是指仅仅只包含一个抽象方法,但是可以有多个非抽象方法的接口。在Java8中接口中可以包含使用`default`或者`static`修饰有方法体的方法。不再局限于之前的，接口中
只能包含`public`、`static`、`final`修饰的变量、`abstract`修饰的方法。  
通常在接口中只包含一个抽象方法，像这样的接口可以被隐式的转换为lambda表达式，但是如果该接口增加了其他的抽象方法，那使用了lambda表达式的就会报错，为了显式的声明该接口就是一个函数式接口所以增加了`@FunctionalInterface`注解。比如java.util.function包下的Function接口就是一个标准的函数式接口。  
```java
@FunctionalInterface
public interface Function<T, R> {

    R apply(T t);

    default <V> Function<V, R> compose(Function<? super V, ? extends T> before) {
        Objects.requireNonNull(before);
        return (V v) -> apply(before.apply(v));
    }

    default <V> Function<T, V> andThen(Function<? super R, ? extends V> after) {
        Objects.requireNonNull(after);
        return (T t) -> after.apply(apply(t));
    }

    static <T> Function<T, T> identity() {
        return t -> t;
    }
}

```

# Lambda表达式
Lambda表达式的语法由参数列表、箭头符号->和函数体组成。函数体既可以是一个表达式，也可以是一个语句块。  
比如：`(int x, int y) -> x + y;`
Lambda是一个匿名函数，使用Lambda会使得代码更加简洁，使用Lambda编写的代码会被编译成一个函数式接口。

## Lambda表达式一些应用的场景
### 循环遍历
1. 外部循环：（1）顺序不可变（2）无法充分使用多核CPU
```java
List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6);
for (int number : numbers) {  
    System.out.println(number);  
} 
```
2. 内部循环：（1）顺序可以不确定（2）可以并行处理
```java
List<Integer> list = Arrays.asList(1, 2, 3, 4);
list.forEach(e -> System.out.println(e));
```

### 参数的传递行为并不仅仅是传值
在Java8之前我认为是可以理解为Java中的传递行为就是值传递的，对于形参和实参可以查看*java基础知识中的形参和实参*。但是Java8中引入了函数式编程之后，参数传递可能不仅仅是值了，还可能是一个函数。


# Optional对象
**`Optional`是用来防止`NullPointException`的工具**，将值包装成Optional.
```java
public final class Optional<T> {
    private static final Optional<?> EMPTY = new Optional<>();

    private final T value;

    private Optional() {
        this.value = null;
    }

    public static<T> Optional<T> empty() {
        @SuppressWarnings("unchecked")
        Optional<T> t = (Optional<T>) EMPTY;
        return t;
    }

    private Optional(T value) {
        this.value = Objects.requireNonNull(value);
    }

    public static <T> Optional<T> of(T value) {
        return new Optional<>(value);
    }

    public static <T> Optional<T> ofNullable(T value) {
        return value == null ? empty() : of(value);
    }

    public T get() {
        if (value == null) {
            throw new NoSuchElementException("No value present");
        }
        return value;
    }

    public boolean isPresent() {
        return value != null;
    }

    public void ifPresent(Consumer<? super T> consumer) {
        if (value != null)
            consumer.accept(value);
    }

    public Optional<T> filter(Predicate<? super T> predicate) {
        Objects.requireNonNull(predicate);
        if (!isPresent())
            return this;
        else
            return predicate.test(value) ? this : empty();
    }

    public<U> Optional<U> map(Function<? super T, ? extends U> mapper) {
        Objects.requireNonNull(mapper);
        if (!isPresent())
            return empty();
        else {
            return Optional.ofNullable(mapper.apply(value));
        }
    }

    public<U> Optional<U> flatMap(Function<? super T, Optional<U>> mapper) {
        Objects.requireNonNull(mapper);
        if (!isPresent())
            return empty();
        else {
            return Objects.requireNonNull(mapper.apply(value));
        }
    }

    public T orElse(T other) {
        return value != null ? value : other;
    }

    public T orElseGet(Supplier<? extends T> other) {
        return value != null ? value : other.get();
    }

    public <X extends Throwable> T orElseThrow(Supplier<? extends X> exceptionSupplier) throws X {
        if (value != null) {
            return value;
        } else {
            throw exceptionSupplier.get();
        }
    }

}
```

# 内置的函数式接口
1. `Function<T, R>`: 接受一个输入参数，返回一个结果
2. `Consumer<T>`: 代表接收一个输入参数并无返回结果的操作
3. `Predicate<T>`: 接收一个输入参数，返回一个布尔结果
4. `Supplier<T>`: 无参数返回一个结果

**在Java8之前使用匿名内部类实现接口**
```java
Function<Integer, String> function = new Function<Integer, String>() {

    @Override
    public String apply(Integer integer) {
        return "hello";
    }
};

System.out.println(function.apply(1));
```
**现在可以使用Lambda表达式实现接口**功能型函数式接口
```java
Function<Integer, String> function = integer -> "hello";
System.out.println(function.apply(1));
```

**Consumer**消费型函数式接口
```java
Consumer consumer = o -> System.out.println(o.toString());
consumer.accept("hello");
```

**Predicate**断言型函数式接口
```java
Predicate predicate = o -> o == null;
predicate.test(null);
```

**Supplier**供给型函数式接口
```java
Supplier supplier = () -> "hello";
System.out.println(supplier.get());
```


# Base64的编码与解码
**在Java8中的`java.util`增加了`Base64`类，用来做Base64的编码与解码**  
具体的用法如下面的代码：
```java
final Base64.Decoder decoder = Base64.getDecoder();
final Base64.Encoder encoder = Base64.getEncoder();
final String text = "字串文字";
final byte[] textByte = text.getBytes("UTF-8");
//编码
final String encodedText = encoder.encodeToString(textByte);
System.out.println(encodedText);
//解码
System.out.println(new String(decoder.decode(encodedText), "UTF-8"));

final Base64.Decoder decoder = Base64.getDecoder();
final Base64.Encoder encoder = Base64.getEncoder();
final String text = "字串文字";
final byte[] textByte = text.getBytes("UTF-8");
//编码
final String encodedText = encoder.encodeToString(textByte);
System.out.println(encodedText);
//解码
System.out.println(new String(decoder.decode(encodedText), "UTF-8"));
```

# Date API
java8 在 `java.time` 包下包含了一个全新的日期和时间处理的API。在时间的处理上强大和方便了很多。      
推荐的文章：[https://zq99299.github.io/java-tutorial/datetime/](https://zq99299.github.io/java-tutorial/datetime/)

1、 在java8之前通常格式化需要使用 `SimpleDateFormat`，该API除了存在线程安全问题外，并且使用时代码不够简洁。       
2、 在java8之前处理时间通常使用Date，比如获取月时需要+1，本月最后一天需要分别按照情况28，29，30，31来进行判断，非常麻烦。

## Clock
自带了时钟类 `Clock` ，可以用来获取某个时区的时间。用来代替 `System.currentTimeMillis()`。也可以获取某个时间点的时间使用 `Instant` 来表示，可以使用 `Instant` 转换为旧版本的 `Date` .    `Clock`是一个抽象类，所以不能创建实例。         
- Clock.offset（Clock，Duration）返回一个被指定持续时间偏移的时钟。
- Clock.systemUTC（）返回表示格林尼治/ UTC 时区的时钟。
- Clock.fixed（Instant，ZoneId）总是返回相同的 Instant。对于这个时钟，时间停滞不前。

```java
System.currentTimeMillis()      // 获取UTC时间的当前时间的毫秒数
Clock.systemUTC().millis()      // 获取UTC时间的当前时间的毫秒数
Clock.systemDefaultZone().millis()      // 获取当前时区下的时间的毫秒数

Clock clock = Clock.systemDefaultZone();
long millis = clock.millis();
System.out.println(millis);
Instant instant = clock.instant();
System.out.println(instant);
Date legacyDate = Date.from(instant); //2019-03-12T08:46:42.588Z
System.out.println(legacyDate);//Tue Mar 12 16:32:59 CST 2019
```

## 日期（LocalDate）相关
日期时间 API 提供四个专门处理日期信息的类，不考虑时间或时区。建议使用这些类：`LocalDate`，`YearMonth`，`MonthDay` 和 `Year`。
- LocalDate : 表示一个确切的日期，可以使用`of()`，`with()` 来进行创建，`LocalDate` 默认格式化的格式是 `yyyy-MM-dd`

```java
LocalDate today = LocalDate.now();      //获取现在的日期
LocalDate tomorrow = today.plus(1, ChronoUnit.DAYS);   // 加一天
LocalDate yesterday = tomorrow.minusDays(2);   // 减2天
LocalDate independenceDay = LocalDate.of(2019, Month.MARCH, 12);        // 创建指定日期
DayOfWeek dayOfWeek = independenceDay.getDayOfWeek();       // 当周的周几

// 2000年11月20日 星期一
LocalDate date = LocalDate.of(2000, Month.NOVEMBER, 20);
// 当前指定日期的下一个 星期三
LocalDate nextWed = date.with(TemporalAdjusters.next(DayOfWeek.WEDNESDAY));
// 当前指定日期的下一个 星期一
LocalDate nextMond = date.with(TemporalAdjusters.next(DayOfWeek.MONDAY));
System.out.println(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL).format(date));       //  2000年11月20日 星期一
```

- YearMonth
```java
YearMonth date = YearMonth.now();
System.out.printf("%s: %d%n", date, date.lengthOfMonth());      // 2018-05: 31
```

- MonthDay
```java
MonthDay date = MonthDay.of(Month.FEBRUARY, 29);        // 2月29号
boolean validLeapYear = date.isValidYear(2010);         // 对于 2010 年是否是有效的时间
```

- Year
```java
boolean validLeapYear = Year.of(2012).isLeap();     // 判断是否是润年
```

## 日期和时间（LocalTime和LocalDateTime）
- LocalTime 
此类对于表示基于人的时间（如电影时间）或本地图书馆的开始和结束时间很有用。 它也可以用来创建数字时钟. 时间格式默认, HH:mm:ss

- LocalDateTime
表示年月日时分秒，实际上是`LocalDate` 和 `LocalTime` 的组合。不存储时区信息。如果要包含时区，必须使用 `ZoneDateTime` 或者 `OffsetDateTime`。

## 时区和偏移（Zone 和 Offset）
time 是其中使用相同的标准时间；每个时区都由一个标识符来描述，并且通常具有格式地区/城市（亚洲/东京）和格林威治/ UTC 时间的偏移量。
```java
// 在中国，是用下面的获取到的就是带时区的北京时间，里面的 offset 就是 +8:00
ZonedDateTime.now()
```

### ZoneId 和 ZondeOffset
Date-Time API中提供了两个用于指定时区和偏移量的类：
- ZoneId 指定时区标识符并提供 Instant 和 LocalDateTime 之间转换的规则。
- ZoneOffset 指定格林威治/ UTC 时间的时区偏移量。

格林威治/ UTC 时间的抵消通常在整个小时内定义，但也有例外。以下代码从 TimeZoneId 示例中打印出使用 Greenwich / UTC 中的偏移量的所有时区的列表， 这些时区不是整点，没有打印的时区 都是整点或则是没有被定义的.

```java
// 获取所有可用的时区
Set<String> allZones = ZoneId.getAvailableZoneIds();

// 按自然顺序排序
// Create a List using the set of zones and sort it.
List<String> zoneList = new ArrayList<String>(allZones);
Collections.sort(zoneList);

LocalDateTime dt = LocalDateTime.now();
for (String s : zoneList) {
    // 获取到的字符串可以通过ZoneId.of获取实例
    ZoneId zone = ZoneId.of(s);
    // 把本地时间加时区信息 转换成一个ZonedDateTime
    // 但是这个LocalDateTime不包含时区信息，是怎么计算出来的呢？本地时间与这个时区相差n小时？
    // 这里的偏移量是针对 格林威治标准时间来说的 +3 ，就是比标准时间快3个小时
    // 如果说一个时区是 +3;而北京是+8，那么该时区比北京慢5个小时
    // 北京时间是12点，那么该时区12-5 = 7
    ZonedDateTime zdt = dt.atZone(zone);
    ZoneOffset offset = zdt.getOffset();
    int secondsOfHour = offset.getTotalSeconds() % (60 * 60);
    String out = String.format("%35s %10s%n", zone, offset);

    // Write only time zones that do not have a whole hour offset
    // to standard out.
    if (secondsOfHour != 0) {
        System.out.printf(out);
    }
}
```

输出
```java
                    America/St_Johns     -03:30
                           Asia/Calcutta     +05:30
                         Asia/Colombo     +05:30
                               Asia/Kabul     +04:30
                      Asia/Kathmandu     +05:45
                        Asia/Katmandu     +05:45
                             Asia/Kolkata     +05:30
                          Asia/Rangoon     +06:30
                              Asia/Tehran     +03:30
                             Asia/Yangon     +06:30
                    Australia/Adelaide     +10:30
                Australia/Broken_Hill     +10:30
                       Australia/Darwin     +09:30
                          Australia/Eucla     +08:45
                         Australia/North     +09:30
                         Australia/South     +10:30
                Australia/Yancowinna     +10:30
             Canada/Newfoundland     -03:30
                             Indian/Cocos     +06:30
                                            Iran     +03:30
                                   NZ-CHAT     +13:45
                        Pacific/Chatham     +13:45
                     Pacific/Marquesas     -09:30
```

### 日期和时间（ZoneDateTime、OffsetDateTime、OffsetTime）
Date-Time API 提供了三个基于时间的类与时区一起工作：
- ZonedDateTime 使用格林威治/ UTC 的时区偏移量处理具有相应时区的日期和时间。
- OffsetDateTime 使用格林威治/ UTC 的相应时区偏移量处理日期和时间，但不包含时区 ID。
- OffsetTime 使用格林威治/ UTC 的相应时区偏移量处理时间，但不包含时区 ID。

#### ZonedDateTime
实际上，结合了 LocalDateTime 与类 了 zoneid 类。它用于表示具有时区（地区/城市，如欧洲/巴黎）的完整日期（年，月，日）和时间（小时，分钟，秒，纳秒）。

下面的代码从 Flight 示例中定义了从旧金山到东京的航班的出发时间，作为美国/洛杉矶时区的 ZonedDateTime。 该 withZoneSameInstant 和 plusMinutes 方法用于创建实例 ZonedDateTime 代表在东京的预计到达时间， 650 分钟的飞行后。该 ZoneRules.isDaylightSavings 方法确定它是否是当飞机抵达东京是否是夏令时。

```java
//        DateTimeFormatter format = DateTimeFormatter.ofPattern("MMM d yyyy  hh:mm a");
        DateTimeFormatter format = DateTimeFormatter.ofPattern("YYYY-MM-dd  HH:mm:ss");

        // Leaving from San Francisco on July 20, 2013, at 7:30 p.m.
        //  2013-07-20  19:30:00
        LocalDateTime leaving = LocalDateTime.of(2013, Month.JULY, 20, 19, 30);
        ZoneId leavingZone = ZoneId.of("America/Los_Angeles");
        ZonedDateTime departure = ZonedDateTime.of(leaving, leavingZone);

        try {
            String out1 = departure.format(format);
            System.out.printf("LEAVING:  %s (%s)%n", out1, leavingZone);
        } catch (DateTimeException exc) {
            System.out.printf("%s can't be formatted!%n", departure);
            throw exc;
        }

        // Flight is 10 hours and 50 minutes, or 650 minutes
        ZoneId arrivingZone = ZoneId.of("Asia/Tokyo");
        // 使用美国洛杉矶出发的时间，然后换算成东京的时区，返回该时区对应的时间
        ZonedDateTime arrival = departure.withZoneSameInstant(arrivingZone)
                .plusMinutes(650); // 在该时区的基础上加650分钟

        try {
            String out2 = arrival.format(format);
            System.out.printf("ARRIVING: %s (%s)%n", out2, arrivingZone);
        } catch (DateTimeException exc) {
            System.out.printf("%s can't be formatted!%n", arrival);
            throw exc;
        }

        // 夏令时
        if (arrivingZone.getRules().isDaylightSavings(arrival.toInstant()))
            System.out.printf("  (%s daylight saving time will be in effect.)%n",
                              arrivingZone);
        else
            // 标准时间
            System.out.printf("  (%s standard time will be in effect.)%n",
                              arrivingZone);
        }
```
总结下：        
1. 首先固定一个不带任何时区的时间
2. 把这个时间加上需要的时区，就标识这个时间就是该时区的
3. 把带时区的时间转换成 目标 时区

#### OffsetDateTime
实际上，结合了 LocalDateTime 与类 ZoneOffset 类。它用于表示格林威治/ UTC 时间的偏移量 （+/-小时：分钟，例如 +06：00 或-）的整个日期（年，月，日）和时间（小时，分钟，秒，纳秒）08:00）。

```java
// 2017.07.20 19:30
LocalDateTime localDate = LocalDateTime.of(2013, Month.JULY, 20, 19, 30);
ZoneOffset offset = ZoneOffset.of("-08:00");

OffsetDateTime offsetDate = OffsetDateTime.of(localDate, offset);

// 当前时间月中的最后一个周4
// 得到的时间是 2017.07.25 19:30 ；时间没有错，但是偏移量有啥用？
OffsetDateTime lastThursday =
        offsetDate.with(TemporalAdjusters.lastInMonth(DayOfWeek.THURSDAY));
System.out.printf("The last Thursday in July 2013 is the %sth.%n",
                  lastThursday.getDayOfMonth());
```

#### OffsetTime
实际上，结合 LocalDateTime 与类 ZoneOffset 类。它用于表示格林威治/ UTC 时间偏移 （+/-小时：分钟，例如+06：00或-08：00）的时间（小时，分钟，秒，纳秒）。 OffsetTime 类是在同一场合的使用 OffsetDateTime 类，但跟踪的日期时不需要。

**总结下时区和偏移量的用法和转换的时候其中两个 api 的区别**     
- withZoneSameInstant : 调用了 toEpochSecond 把当前的时间纳秒 结合 指定的偏移量换算成新的纳秒
- withZoneSameLocal ：不会换算时间，只是把时区更改了

```java
// 一个不带任何时区的时间
LocalDateTime date = LocalDateTime.of(2018, 05, 01, 0, 0, 0);

ZonedDateTime d1 = ZonedDateTime.of(date, ZoneId.systemDefault());

ZoneOffset offset = ZoneOffset.of("+08:00");
OffsetDateTime d2 = OffsetDateTime.of(date, offset);

// 2018-05-01T00:00+08:00[GMT+08:00]
// ZoneId 带了具体的ID
System.out.println(d1);
// 2018-05-01T00:00+08:00
// 而偏移没有ID,因为多个ID对应的值有可能是一样的
System.out.println(d2);

// 那么把中国时间变成其他的时间
// 2018-04-30T20:00+04:00[Asia/Yerevan]
// 把该时间转换成指定时区了
d1.withZoneSameInstant(ZoneId.of("Asia/Yerevan"));
// 2018-05-01T00:00+04:00[Asia/Yerevan]
// 只是改变了时区
d1.withZoneSameLocal(ZoneId.of("Asia/Yerevan"));
```


